# General Information

Here you can find some general information of the Boysen-TU Dresden-Research Training Group PhD project "F4: Monitoring Concept: Sustainability of the Hydrogen Production" by Christian Gutsche.

The work of the projects is organized using the following working packages:
![](https://md.inf.tu-dresden.de/notes/uploads/43016fef-45d0-45d3-83e3-06904805f186.png)

The Code of PNRG Library that uses Petri Nets for Energy park simulation can be found [here](https://git-st.inf.tu-dresden.de/cgutsche/PNRG).

In [Posters](https://git-st.inf.tu-dresden.de/cgutsche/general/-/tree/main/Posters) the Poster presented at the Boysen-TU Dresden-RTG Autumn Colloquium 2023 and its literature list can be found. 