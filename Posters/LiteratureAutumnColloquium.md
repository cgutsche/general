# Literature Autumn Colloqium Poster

[1] Modelica Specification [modelica.org/documents/MLS.pdf](https://modelica.org/documents/MLS.pdf) [viewed on August 28th 2023).

[2] Fridgen, Gilbert et al. [2020). “A holistic view on sector coupling”. In: Energy Policy 147, p. 111913. DOI: [10.1016/j.enpol.2020.111913](https://doi.org/10.1016/j.enpol.2020.111913).

[3] Liu, Tao et al. [2022). “Stability and Control of Power Grids”. In: Annual Review of Control, Robotics, and Autonomous Systems 5. DOI: [10.1146/annurev-control-042820-011148](https://www.annualreviews.org/doi/10.1146/annurev-control-042820-011148).

[4] Tinnerholm, John [2022). A Composable and Extensible Environment for Equation-based Modeling and Simulation of Variable Structured Systems in Modelica. Linköping UniversityLinköping University, Software and Systems, Faculty of Science Engineering. Licentiate Thesis. DOI: [10.3384/9789179293680](http://liu.diva-portal.org/smash/record.jsf?pid=diva2%3A1662273&dswid=-4090).

[5] Petri, Carl Adam [1962). "Kommunikation mit Automaten". Technische Hochschule Darmstadt. URL: [Link](https://edoc.sub.uni-hamburg.de/informatik/volltexte/2011/160/).

[6] Murata, Tadao [1989). “Petri nets: Properties, analysis and applications”. In: Proceedings of the IEEE 77.4, pp. 541–580. DOI: [10.1109/5.24143](https://ieeexplore.ieee.org/document/24143).

[7] Agerwala, Tilak [1974). Complete Model for Representing the Coordination of Asynchronous Processes. The JohnsHopkins University, Baltimore, Maryland

[8] David, René and Hassane Alla [1987). “Continuous Petri nets”.In: 8th European Workshop on Application and Theory of Petri Nets, Zaragoza, pp. 275–294.  

[9] David, René and Hassane Alla [2008-07). “Discrete, Continuous, and Hybrid Petri Nets”. In: Control Systems Magazine,IEEE 28, pp. 81–84. DOI: [10.1109/MCS.2008.920445](https://ieeexplore.ieee.org/document/4518908).

[10] Mai, Carl et al. [2018-02). “Adaptive Petri Nets -A Petri Net Extension for Reconfigurable Structures”. In: Proceedings of the Tenth International Conference on Adaptive, Self-Adaptive Systems, and Applications. URL: [Link](https://www.researchgate.net/publication/334645658_Adaptive_Petri_Nets_-A_Petri_Net_Extension_for_Reconfigurable_Structures).

[11] Christian Gutsche et al [2023). „PNRG – A Library for Modeling Variable Structure Energy Grids in Modelica using Energetic Petri Nets” (Accepted at International Modelica Conference, not published yet)